#!/bin/bash
###############################################################################
#                                                                             #
# twee2latex - converts twee .tw files to latex .tex files                    #
#                                                                             #
# This converts .tw files in the twee directory to .tex files in the latex    #
# directory.                                                                  #
#                                                                             #
# See Changelog and Todo for details of where this is currently               #
#                                                                             #
# Copyright 2019 Jigme Datse Yli-Rasku and Datse Multimedia Productions       #
#                                                                             #
###############################################################################

maindir="/home/jrasku/Documents/Writing/come-back"
tweedir="${maindir}/twee"
latexdir="$maindir/latex"

cd ${latexdir}
./newsection.sh $1
cat ${tweedir}/$1.tw >> ${latexdir}/$1-body.tex

vi ${latexdir}/$1-body.tex

