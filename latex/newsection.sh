cp template/main.tex $1.tex
cp template/main-head.tex $1-head.tex
cp template/main-body.tex $1-body.tex

cp makefile makefile.old

echo -e >> makefile
echo -e \$${1}tex = $1.tex $1-head.tex \\ >> makefile
echo -e "\t$1-body.tex" >> makefile
echo -e >> makefile
echo -e $1 : \$\(${1}tex\) >> makefile
echo -e "\tlatex $1" >> makefile
echo -e "\tlatex $1" >> makefile
echo -e "\tdvips $1" >> makefile
echo -e "\tgs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$1.pdf $1.ps" >> makefile

vi makefile

vi $1*.tex

echo -e >> main.tex
echo -e "chapter{ChapterName}" >> main.tex
echo -e "This is just some placeholder text so the chapter will have some text" >> main.tex
echo -e "\\input{$1-head}" >> main.tex
echo -e "\\input{$1-body}" >> main.tex
echo -e >> main.tex

vi main.tex
