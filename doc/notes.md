# Notes on this project

I think that we should probably look at some things here:

* Twine Source
* Moving to LaTeX
* Reasoning for that
* Notes on the LaTeX version

## Twine Source

Some time ago (from when I started this project in this form) I created 
this as an "interactive non-fiction" in Twine.  

There were kind of two different reasons for that:

* Working with Twine to learn how to do things
* Trying to find a tool that allows us to work the way we like

So, it kind of made sense at the time, but with recent reading I have 
decided that while maybe something interactive, or non-linear would work
for this project, it doesn't really work that well.

### Using Twine

I really wanted to be able to create an interactive fiction, or a game in 
Twine, and I think in a lot of ways that may continue to be a goal.  
Between this, and a couple of other (I think) projects that I have worked 
on I can really say that I probably could get Twine to work reasonably 
well for what I want to do with that.

Unfortunately, I don't really like how it works, and I've kind of realized
that probably I don't want to write stories, or more commonly non-fiction 
with the kind of game engine.  I more like to develop games that are less
story driven.  

### Why Twine didn't really work

It was really good because I could refer to a section and just have it 
automagically create the new section, or just link to it.  This felt like
it was a really good thing to have, but I ended up with about 50 sections
most of them not written, and if I wanted to work with it outside of the
Twine app, it ended up just a real problem, as that was orrignally a 
single file.  

It *can* work with multiple files, and then be compiled into the "game"
which the end result is, but a lot of what I liked pretty much relied on
working with it in Twine and not working with the Twee source.

It's kind of a case that you take advantage of the editor tools, but then
it is a pain if you want to work with it outside the editor.  Or you work
entirely outside of the editor.  

It was an interesting experience, and in a lot of ways it seems like it 
should be possible to change how it works so that it doesn't just work 
with a single large file which is hard to work with, or separate files,
but no way of doing it in any kind of IDE.

Of course, I'll probably at some point go back to it.  

## Moving to LaTeX

I mentioned that I didn't really feel that this (for now) fits the ideas
around the interactive fiction environment.  I felt that for the most 
part it didn't have any reason to be interactive/non-linear.

So, I decided that I wanted to focus on what I hope will be a print book
in the end, and I've worked with LaTeX and previously TeX and decided that
I could work quite well with that.  It also (I have found) a good way to 
work with multiple files.  

## Why we like LaTeX

We realize we could work with a lot of other tools, and in a lot of ways, 
we could be better off.  But the tools we have tried, all feel like even 
when it's not really locking you into the tool, it kind of works such that
working with it differently, often will result in some things broken.

In converting this over, I decided to work with Eclipse to break the big
.tw file, into separate files for each section that we are currently 
working with to convert to LaTeX files.  But I tend to like to work with
pretty basic tools (like vi right now), and scripts that I write myself.

So I'm now writing most of this in vi, and just writing some scirpts to
make things work better.  

## Notes on the LaTeX version

This was kind of the main thing that we wanted to have some thoughts on
what we want to do with this.  A lot of this probably is already in there
as I've tried to write some of this already.

So there are a few different things that we probably want to talk about

* Twee features which we want to handle
* Indexing
* References to sources
* Including smaller sections (not whole articles).

### Twee Features

The big one is that I could have a reference [[reference]] which would go 
to a different section, and I don't really have a way to handle that.  

That is kind of the main feature.  Currently I've not got a good way to 
handle that, because I can't really do a link to a new section.

There are other features which I had to create a "script" to handle, and
most of that comes to includes, and how that gets handled.  

### Indexing

I can write an index, and currently I'm kind of trying to do sort of 
automatically or more not automatically (because that's really difficult)
but just so that I can quickly do it.  

My current approach is not really working, but I can't really be sure how
to better handle it until I look at the source.

The issue which I am running into with what I've done, is while it kind 
of handles what I want for now (giving me a place where I have mentions
of certain words/conecepts) but it's not really meaningful with the end
version.  

I'm looking at probably breaking that down a bit to multiple indexes, or
something.  One which really points to where you can read about various
concepts, and one which points to ones which mention those concepts.

So, I'm thinking that I probably need that second one to have entries 
which indicate a "primary reference" and one which is, "talks about it,
but isn't really a good overview..."

Currently we have only one index, and that's mostly pointing to where we 
refer to other sections.

### References to Sources

### Inclues of smaller sections
